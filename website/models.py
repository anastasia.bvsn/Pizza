from django.db import models
from datetime import datetime


class Dish(models.Model):
    MEAT = 'Meat'
    FISH = 'Fish'
    VEG = 'Vegetarian'
    kind_choices = (
        (MEAT, 'meat'),
        (FISH, 'fish'),
        (VEG, 'veg')
    )
    title = models.CharField(max_length=100)
    image = models.FileField(null=True, blank=True)
    price = models.IntegerField(default=0)
    kind = models.CharField(max_length=10, choices=kind_choices, default=MEAT)

    class Meta:
        verbose_name = 'Dish'
        verbose_name_plural = 'Dishes'


class OrderItems(models.Model):
    dish = models.OneToOneField(Dish, on_delete=models.SET_NULL, null=True)
    qty = models.PositiveIntegerField(default=1)
    is_ordered = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Order items'
        verbose_name_plural = 'Order items'

    def __str__(self):
        return '{}, {}'.format(self.dish.title, self.qty)


class Cart(models.Model):
    ordered_items = models.ManyToManyField(OrderItems, blank=True)
    is_ordered = models.BooleanField(default=False)
    date = models.DateTimeField(default=datetime.now, blank=True)


class Order(models.Model):
    NEW = 'Accepted'
    WAY = 'Given to the courier'
    DONE = 'Delivered'
    CANC = 'Cancelled'
    status_choices = (
        (NEW, 'Accepted'),
        (WAY, 'Given to the courier'),
        (DONE, 'Delivered'),
        (CANC, 'Cancelled')
    )
    name = models.CharField(max_length=50)
    adress = models.CharField(max_length=100)
    phone = models.IntegerField()
    items = models.ForeignKey(Cart, null=True, on_delete=models.SET_NULL)
    total = models.IntegerField(default=0)
    date = models.DateTimeField(default=datetime.now, blank=True)
    status = models.CharField(max_length=50, choices=status_choices, default=NEW)

    class Meta:
        verbose_name = 'Order'
        verbose_name_plural = 'Orders'

    def __str__(self):
        return 'Order #{}'.format(self.id)
