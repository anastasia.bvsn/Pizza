from django import forms
from website.models import Order


class OrderForm(forms.ModelForm):

    class Meta:
        model = Order
        fields = ('name', 'adress', 'phone')
