// Ajax block using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }
});

// Ajax for the main page
$(document).ready(function() {
  $(document).on('click', '.btn-add', function(ev) {
    var button = ev.target;
    var dish_id = button.getAttribute("data-dish-id");
    $.ajax({
      url: 'add_to_cart/' + dish_id,
      type: 'POST',
      dataType: "json",
      data: {
        "dish_id" : dish_id
      },
      success: function(response) {
        console.log(response);
        $('#cart-total-price').text(response.total + ' ₴');
        $('#cart-items-amount').text('Dishes in cart: ' + response.cart_items_count);
      },
      error: function(error) {
        console.log(error);
      }
    });
  });
})

// Ajax for cart
$(document).ready(function() {
  $(document).on('click', '.plus', function(ev) {
    var plus_button = ev.target;
    var dish_id = plus_button.getAttribute("data-id");
    console.log(plus_button);
    $.ajax({
      url: '/increase_quantity/' + dish_id,
      type: 'POST',
      dataType: "json",
      data: {
        "dish_id" : dish_id
      },
      success: function(response) {
        console.log(response);
        // Searching dish in cart with id=dish_id to change its qty:
        $('span[data-id^='+response.id+']').text(response.quantity);
        // Total price in cart:
        $('th[colspan^=2]').text(response.total + ' ₴');
      },
      error: function(error) {
        console.log(error);
      }
    });
  });
})


$(document).ready(function() {
  $(document).on('click', '.minus', function(ev) {
    var minus_button = ev.target;
    var dish_id = minus_button.getAttribute("data-id");
    console.log(minus_button);
    $.ajax({
      url: '/decrease_quantity/' + dish_id,
      type: 'POST',
      dataType: "json",
      data: {
        "dish_id" : dish_id
      },
      success: function(response) {
        console.log(response);
        // Searching dish in cart with id=dish_id to change its qty:
        var qty = $('span[data-id^='+response.id+']').text(response.quantity);
        if (parseInt(qty.text(), 10) <= 1) {
          $(minus_button).closest("tr").remove();
        };
        // Total price in cart:
        // $('th[colspan^=2]').text(response.total + ' ₴');
        $('.js-cart-total').text(response.total + ' ₴');
      },
      error: function(error) {
        console.log(error);
      }
    });
  });
})


//------------------------------------------------------------

function queryImageKinds() {
  var kinds = document.querySelectorAll('div.dish-kind');
  for (var i = 0; i < kinds.length; i++) {
    var kind = kinds[i];
    var kind_text = kind.innerHTML;
    var img = kind.parentElement.querySelector('img');

    switch (kind_text) {
      case 'Meat':
        img.src='/static/images/meat.png';
        break;
      case 'Fish':
        img.src='/static/images/fish.png';
        break;
      case 'Vegetarian':
        img.src='/static/images/veg.png';
        break;
      default:
        console.log("Kind == default, image deleted")
        img.remove();
    }
  }
}


// Toggle visibility of dishes' kinds tooltips on hovering on their images:
var img = document.querySelectorAll('div.dish-footer > img');
console.log(img);
for (var i = 0; i < img.length; i++) {
  img[i].addEventListener('mouseover', function(ev) {toggleVisibility(ev.target.parentElement.querySelector('div.dish-kind'))} );
  img[i].addEventListener('mouseout', function(ev) {makeInvisible(ev.target.parentElement.querySelector('div.dish-kind'))} );
}

function toggleVisibility(el) {
  el.style.display = (el.style.display == 'none' || el.style.display == '') ? 'block' : 'none';
}

function makeInvisible(el) {
  el.style.display = (el.style.display != 'none') ? 'none' : {};
}

queryImageKinds();
